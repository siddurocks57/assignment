/**
 * Global WebService calling function
 */
function callAppDemoServiceAsync(inputparams, callBackAsync) {

    try {
        kony.print("appConfig.appVersion ---" + JSON.stringify(appConfig));
        inputparams.appID = appConfig.appId;
        inputparams["channel"] = "rc";
        inputparams.httpheaders = {};
        kony.print("input param Before calling service :::: ---" + JSON.stringify(inputparams));
        kony.print("Before "+inputparams.serviceID+ " Service Call Time:::: "+kony.os.date());
        var resulttable = kony.net.invokeServiceAsync(gblURL, inputparams, callBackAsync);
        
    } catch (err) {
        kony.print("Utils callAppDemoServiceAsync"+ err);
    }
}


/**
 * To load loading screen 
 * 
 */
function showSyncLoadingScreen(text){
    
	text = " " + text + " \n"; 
    var loaderText=" "+kony.i18n.getLocalizedString("loading")+"\n";

	kony.application.showLoadingScreen("", loaderText,constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, true, null)
}


/**
 * To dismiss loading screen 
 */
function dismissSyncLoadingScreen(){
	kony.application.dismissLoadingScreen();
}


/**
 * To check the network is available
 * @returns {}the Network State.
 */
function isNetworkAvailable() {
    return kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY);
    
}