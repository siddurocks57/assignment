
 /**
 * Function to call to get all Categories List from the Best Buy.
 */
function callGetCategoiresServiceAsync(){
    
  	try{

		kony.print("Calling callAppDemoServiceAsync !!!! ")  	
		kony.print("appconfig "+JSON.stringify(appConfig))
		if(isNetworkAvailable()){		
			var inputparams_Categoriess = {};
			inputparams_Categoriess["serviceID"]= "getCategories"; 
			inputparams_Categoriess["categoryID"]= "cat00000"; 
			showSyncLoadingScreen("Loading..");
			callAppDemoServiceAsync(inputparams_Categoriess,categoriesCallBack); 	 
			kony.print("time in Categories" + kony.os.date())
			
		}else{
		    kony.print("@@@@@@@@@@@@@@@@ network is not avialble @@@@@@@@@@@@@@@@ ");
			alert("Please check your Internet Connection");
		
        	return;	
		}
	}catch (e) {
		kony.print("CategoriesService"+e);
	}	 
}


/**
  * Call back function for callGetCategoiresServiceAsync()
  * @param {string} status.
  * @param {table} ResultTable.
  * @return 
  */  
function categoriesCallBack(status, resulttable){	

	try{
		   
		if(status==400){
		 if(resulttable!=null){		
				if(resulttable["opstatus"]==0){
					kony.print("After Service Call Time:::: "+kony.os.date());			  
				    dismissSyncLoadingScreen();
					
					var resultData=resulttable["ResultData"];
					 kony.print("ResultData---- "+resultData.length);
					 var categoriesList=[];
					if(resultData.length >0){
					 for(var i=0;i<resultData.length;i++){
				    		kony.print(" ::::::: "+resultData[i]["categoryName"]) 
				    		 var data = {
								            "lblProdcutsName":resultData[i]["categoryName"] ,
								            "lblProdcutsID": resultData[i]["categoryID"]
									     }
							categoriesList.push(data);		 
				    	}  // for loop end
                      
				    	frmHome.segCategories.setData(categoriesList);
					   
					}else{
					   dismissSyncLoadingScreen();
					   kony.print("resultData is zero");
					}
					
				}else{
				    dismissSyncLoadingScreen();
					kony.print("opstatus is not equal to zero");
				}
			}else{
					dismissSyncLoadingScreen();
					kony.print("when resultTable is Zero");
			}
		}// end if status
	}catch(err){
		kony.print("@@@@@@@@@@@@@@@@ Exception categoriesCallBack -  : "+err);
	}
}// end of function
